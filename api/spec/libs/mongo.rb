require "mongo" #Aqui está fazendo a conexão com o banco de dados(MongoDB)

Mongo::Logger.logger = Logger.new("./logs/mongo.log") #Colocando os logs do Mongo na pasta libs

class MongoDB
  attr_accessor :client, :users, :equipos

  def initialize
    #client = Mongo::Client.new('mongodb://127.0.0.1:27017/test') - Código do MONGO
    #Só alterei o ip pro container do banco(rocklov-db) e o nome do banco(rocklov)
    @client = Mongo::Client.new("mongodb://rocklov-db:27017/rocklov")
    #Código acima do MONGO: chama o módulo do mongo, e do módilo chama a classe Client, que ativa uma instancio de conexão
    @users = client[:users] #users é dos usuários do banco, do qual eu vou remover o email
    @equipos = client[:equipos]  #equipo é a tabela que eu estou manipulando, no caso quero remover
  end

  def drop_danger #método de exclusão dos usuários
    client.database.drop
  end

  def insert_users(docs) #Metodo insere os usuário no banco
    @users.insert_many(docs)
    #Many está inserindo vários

  end

  def remove_user(email)
    users.delete_many({ email: email }) #Está deletando do banco de dados antes de preencher o cadastro
  end

  def get_user(email)
    #busca o usuário pelo e-mail
    user = users.find({ email: email }).first
    return user[:_id] #devolve o ID do usuário
  end

  def remove_equipo(name, user_id)
    obj_id = BSON::ObjectId.from_string(user_id)
    #Acima, converte o user_id que está como string, para objectid(que é aceito no mongo)
    @equipos.delete_many({ name: name, user: obj_id }) #aqui deleta o user_Id tbm
  end

  def get_mongo_id
    return BSON::ObjectId.new #Vai criar um object id aleatorio
  end
end
