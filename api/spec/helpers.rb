module Helpers
  def get_fixture(item)
    YAML.load(File.read(Dir.pwd + "/spec/fixtures/#{item}.yml"), symbolize_names: true)
    #este código acima, carrega a massa num arquivo YAML
  end

  def get_thumb(file_name)
    return File.open(File.join(Dir.pwd, "spec/fixtures/images", file_name), "rb") #Abre a imagem
    #file_name está encapsulado dentro do método
    #Rb quer dizer somente leitura no formato binário. Ele encaminha todas as informações do thumbnail
  end

  module_function :get_fixture
  module_function :get_thumb
end
