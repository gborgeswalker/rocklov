require "httparty"

class Baseapi
  include HTTParty # Através deste include, a classe se torna o próprio HTTParty
  base_uri "http://rocklov-api:3333"
end
