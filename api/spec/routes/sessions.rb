require_relative "base_api"

class Sessions < Baseapi #Está herdando de Baseapi
  def login(payload)
    return self.class.post(
             #Self.class deixa ter acesso aos objetos da própria classe
             "/sessions",
             body: payload.to_json, #to_json converte para o formato JSON, porque o Ruby trabalha com HASHES
             headers: {
               "Content-Type": "application/json",
             #Requisição POST pra fazer login na API atraves do ruby, rspec e httparty
             },
           )
  end
end
