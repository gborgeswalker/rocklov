#encoding: UTF-8
describe "DELETE /equipos/{equipo_id}" do
  before(:all) do
    payload = { email: "to@mate.com", password: "pwd123" }
    #Aqui está logando, pra posteriormente pegar o ID e cadastrar o equipo

    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"] #Pega o ID do Usuário
  end

  context "obter unico equipo" do
    before(:all) do
      # dado que eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("pedais.jpg"),
        #pega o método get_thumb(que está em helpers.rb), coloca o calor que eu quero e retorna a abertura do arquivo
        name: "Pedais do Tom Morello",
        category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"), #Por causa do acentoo está dando ruim, força pra ASCII
        price: 199,
      }

      MongoDB.new.remove_equipo(@payload[:name], @user_id)
      #E eu tenho o ID do equipamento
      equipo = Equipos.new.create(@payload, @user_id)
      @equipo_id = equipo.parsed_response["_id"]
      #puts @result

      #quando faço uma requisição DELETE por id
      @result = Equipos.new.remove_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end

  context "equipo nao existe" do
    before(:all) do
      @result = Equipos.new.remove_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end
end
