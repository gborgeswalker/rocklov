describe "Post /sessions" do
  context "login com sucesso" do
    before(:all) do
      #O before roda uma vez para cada IT, para resolver..
      # Uso o (:all), pra rodar apenas uma vez
      payload = { email: "betao@hotmail.com", password: "pwd123" }
      @result = Sessions.new.login(payload)
      #Classe Sessions está o httparty,
      # Método login recebe o payload e os argumentos acima
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24 #Length conta a quantidade de itens no string
      #Parsed tranforma em HASH
    end
  end

  #examples = [
  # {
  #   title: "senha incorreta",
  #   payload: { email: "gborges@gmail.com", password: "123456" },
  #   code: 401,
  #   error: "Unauthorized",
  # },
  # {
  #   title: "usuario não existe",
  #   payload: { email: "404@gmail.com", password: "123456" },
  #   code: 401,
  #   error: "Unauthorized",
  # },
  # {
  #   title: "email em branco",
  #   payload: { email: "", password: "123456" },
  #   code: 412,
  #   error: "required email",
  # },
  # {
  #   title: "sem o campo email",
  #   payload: { password: "123456" },
  #   code: 412,
  #   error: "required email",
  # },
  # {
  #   title: "senha em branco",
  #   payload: { email: "gborges@gmail.com", password: "" },
  #   code: 412,
  #   error: "required password",
  # },
  # {
  #   title: "sem o campo senha",
  #   payload: { email: "gborges@gmail.com" },
  #   code: 412,
  #   error: "required password",
  # },
  #]

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    #examples é um array. Argumento |e| representa um exemplo por vez
    context "#{e[:title]}" do
      #contexto vai ter o titulo de cada exemplo
      before(:all) do
        #O before roda uma vez para cada IT, para resolver..
        # Uso o (:all), pra rodar apenas uma vez

        @result = Sessions.new.login(e[:payload])
        #Classe Sessions está o httparty,
        # Método login recebe o payload. Pego o paylode do Array
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code] #Pega o código de erro do array
      end

      it "valida id do usuario" do
        expect(@result.parsed_response["error"]).to eql e[:error] #Pega o erro do array
        #Parsed tranforma em HASH
      end
    end
  end
end
