describe "Post /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email]) #remove o email do Banco
      @result = Signup.new.create(payload)
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24 #Length conta a quantidade de itens no string
      #Parsed tranforma em HASH
    end
  end

  examples = Helpers::get_fixture("signup")

  examples.each do |e|
    #examples é um array. Argumento |e| representa um exemplo por vez
    context "#{e[:title]}" do
      #contexto vai ter o titulo de cada exemplo
      before(:all) do
        #O before roda uma vez para cada IT, para resolver..
        # Uso o (:all), pra rodar apenas uma vez
        Signup.new.create(e[:payload]) # Cadastra o usuário no banco
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code] #Pega o código de erro do array
      end

      it "deve retornar mensagem: #{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error] #Pega o erro do array
        #Parsed tranforma em HASH
      end
    end
  end
end

#Código de antes do desafio
# context "usuario ja existe" do
#   before(:all) do
#     # dado que eu tenho um novo usuário
#     payload = { name: "João da Silva", email: "joao@ig.com.br", password: "pwd123" }
#     MongoDB.new.remove_user(payload[:email]) #remove o email do Banco
#     # E o e-mail desse usuário já foi cadastrado no sistema
#     Signup.new.create(payload) # Aqui vai cadastrar e ter status 200

#     # Quando faço uma requisição para a rota /signup
#     @result = Signup.new.create(payload) #aqui vain dar o 409
#   end

#   it "deve retornar 409" do
#     #Então deve retornar 409
#     expect(@result.code).to eql 409
#   end

#   it "deve retornar mensagem" do
#     expect(@result.parsed_response["error"]).to eql "Email already exists :("
#     #Parsed tranforma em HASH
#   end
# end
