describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "ed@gmail.com", password: "pwd123" }
    #Aqui está logando, pra posteriormente pegar o ID e cadastrar o equipo

    result = Sessions.new.login(payload)
    @ed_id = result.parsed_response["_id"] #Pega o ID do Usuário
  end

  context "solicitar locacao" do
    before(:all) do
      # dado que Joe Perry tem uma "Fender Stato" para locação

      result = Sessions.new.login({ email: "joe@gmail.com", password: "pwd123" })
      # Em cima loga com a conta do Joe e armazena em result
      joe_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        #pega o método get_thumb(que está em helpers.rb), coloca o calor que eu quero e retorna a abertura do arquivo
        name: "Fender Stato",
        category: "Cordas",
        price: 150,
      }
      MongoDB.new.remove_equipo(fender[:name], joe_id) #remove o equipamento

      result = Equipos.new.create(fender, joe_id)
      fender_id = result.parsed_response["_id"] #Pega o ID do equipamento cadastrado

      # quando solicito a locação do Joe Perry
      @result = Equipos.new.booking(fender_id, @ed_id)
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
