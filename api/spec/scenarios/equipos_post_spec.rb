describe "Post /equipos" do
  before(:all) do
    payload = { email: "to@mate.com", password: "pwd123" }
    #Aqui está logando, pra posteriormente pegar o ID e cadastrar o equipo

    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"] #Pega o ID do Usuário
  end

  context "novo equipo" do
    before(:all) do
      payload = {
        thumbnail: Helpers::get_thumb("kramer.jpg"),
        #pega o método get_thumb(que está em helpers.rb), coloca o calor que eu quero e retorna a abertura do arquivo
        name: "Kramer Eddie Van Halen",
        category: "Cordas",
        price: 299,
      }

      MongoDB.new.remove_equipo(payload[:name], @user_id)

      @result = Equipos.new.create(payload, @user_id)
      #puts @result
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end

  context "nao autorizado" do
    before(:all) do
      payload = {
        thumbnail: Helpers::get_thumb("baixo.jpg"),
        #pega o método get_thumb(que está em helpers.rb), coloca o calor que eu quero e retorna a abertura do arquivo
        name: "Contrabaixo",
        category: "Cordas",
        price: 59,
      }

      @result = Equipos.new.create(payload, nil) #nil é nulo no ruby
      #puts @result
    end
    it "deve retornar 401" do
      expect(@result.code).to eql 401
    end
  end
end
