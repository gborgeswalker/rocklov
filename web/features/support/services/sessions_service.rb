require_relative "base_service"

class SessionsService < BaseService #Está herdando de BaseService
  def get_user_id(email,password) #Método retorna o user id fazendo login via api
    payload ={ email: email, password: password}
    result = self.class.post(
             #Self.class deixa ter acesso aos objetos da própria classe
             "/sessions",
             body: payload.to_json, #to_json converte para o formato JSON, porque o Ruby trabalha com HASHES
             headers: {
               "Content-Type": "application/json",
             #Requisição POST pra fazer login na API atraves do ruby, rspec e httparty
             },
           )

           return result.parsed_response["_id"]
  end
end
