
class Alert
    include Capybara::DSL

    def dark
        return find(".alert-dark").text #Com o find eu procuro campo, e armazeno na variável alert
        #Retorno o find
        # .text retorna o texto do seletor CSS .alert_dark
    end

end