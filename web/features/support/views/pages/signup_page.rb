

class SignupPage
    include Capybara::DSL

    def open
        visit "/signup"
    end

    def create(user) 
        #Só recebe USER como argumento, por que já está sendo usando no código
        find("#fullName").set user[:nome]
        find("#email").set user[:email]
        find("#password").set user[:senha]

    click_button "Cadastrar"
    end

end