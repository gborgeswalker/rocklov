

class DashPage
    include Capybara::DSL
    
    def on_dash?
        # Interrogação (?), retorna verdadeiro ou falso
        page.has_css?(".dashboard")
    end

    def goto_equipo_form
        click_button "Criar anúncio"
    end

    def equipo_list
        return find(".equipo-list")

    end

    def has_no_equipo?(name) #Metodo true or false para ver se tem equipo
     return page.has_no_css?(".equipo-list li", text: name)
      #has_no_css? método que retorna V ou F, e pergunta se ná página não contem um determinado CSS
    end 

    def request_removal(name) #faz a exclusão do equipamento
        equipo = find(".equipo-list li", text: name)
        #acha a lista que está o equipo, e busca pelo texto, no caso o nome do equipo
        equipo.find(".delete-icon").click
        # Faz uma busca dentro do elemento que eu capturei na variável equipo
    end

    def confirm_removal  #Confirma a exclusão do equipo
        click_on "Sim"
    end

    def cancel_removal  #Cancela a exclusão do equipo
        click_on "Não"
    end

    def order #Retorna a notificação no Dashboard
        return find(".notifications p")
    end

    def order_actions(name) #Retorna o css dos botões ACEITAR e REJEITAR
        return page.has_css?(".notifications button", text: name)

    end
end