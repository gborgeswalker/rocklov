

class LoginPage
    include Capybara::DSL
    #Está falando pro Ruby que a Classe precisa conhecer todos os recursos do capybara
    def open
        visit "/" 
    end

    def with(email, password)
        find("input[placeholder='Seu email']").set email
        find("input[type=password]").set password
        click_button "Entrar" 
    end

end