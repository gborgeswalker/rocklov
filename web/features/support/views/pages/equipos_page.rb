

class EquiposPage
    include Capybara::DSL

    def create(equipo)
        #expect(page).to have_css "#equipoForm" # Está validado se fui direcionado para página de cadastro de equipamentos
        # checkpoint com timeout explicito
        page.has_css?("#equipoForm") # ? retorna verdadeiro ou falso
        # Não pode usar o expect no PageObjetic, usar igual acima.
        #Porque não está fazendo uma validação através do expect, mas usa o timeout do capybara pra esperar o elemento esta visivel
        
        upload(equipo[:thumb]) if equipo[:thumb].length > 0
        #Se a quantidade de letras na string for maior que ZERO ele executa
        # Length é um método que consegue obter a quantidade de iten num array ou letras no array

        find("input[placeholder$=equipamento]").set equipo[:nome] #O cifrão $ no css significa "termina com"
        select_cat(equipo[:categoria]) if equipo[:categoria].length > 0
        find("input[placeholder^=Valor]").set equipo[:preco] # o ^ é um seletor CSS para "começa com..""
   
        click_button "Cadastrar"
    end

    def select_cat(cat)
        #Como no teste negativo não dar para deixar uma categoria vazia, criamos este método
        find("#category").find('option', text: cat).select_option
        #Acima, busca o elemento pai (category), e escolhe a opção que tem o texto cordas com o select_option
    end

    def upload(file_name)

        thumb = Dir.pwd + "/features/support/fixtures/images/" + file_name
        #Dir.pwd vai mostrar o caminho do projeto (Que é onde estou)
        # Assim passa o caminho relativo e contatena com a variavel da thumb, que tem o valor da imagem

        find("#thumbnail input[type=file]", visible: false).set thumb  #visible:false busca o elemento mesmo que estiver oculto                             
       
        
    end

end