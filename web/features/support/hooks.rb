# Vai ser criado um gancho de inicialização do cenário

Before do
  @alert = Alert.new
  @login_page = LoginPage.new #Carrega o page em memória, pra não precisar chamar a classe
  @signup_page = SignupPage.new
  @dash_page = DashPage.new
  @equipos_page = EquiposPage.new

  #page.driver.browser.manage.window.maximize
  page.current_window.resize_to(1440, 900) #Quando abrir o browser, a resolcução será essa
end

After do
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")

  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),
  )
end
