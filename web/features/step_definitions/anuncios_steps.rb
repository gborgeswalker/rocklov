Dado("Login com {string} e {string}") do |email, password|
  @email = email

  @login_page.open
  @login_page.with(email, password) # Email e password vem da feature
  # Checkpoint para garantir que estamos no Dashboard
  expect(@dash_page.on_dash?).to be true
  # Espera que o que o método "on_dash" retorno seja verdadeiro
end

Dado("que acesso o formulario de cadastro de anúncios") do
  @dash_page.goto_equipo_form #Clica no criar anuncio. A classe que chama está em dash_page
end

Dado("que eu tenho o seguinte equipamento:") do |table|
  @anuncio = table.rows_hash #rows_hash pega uma tabela de chave e valor e converte pra um objeto do ruby
  #@ serve para fazer a variavel global, ou de instancia
  #log @anuncio

  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
  #remove o nome e e-mail do dono do anúncio
end

Quando("submeto o cadastro desse item") do
  @equipos_page.create(@anuncio)
  #@anuncio tem toda a massa, pra preenchimento do formulário

end

Então("devo ver esse item no meu Dashboard") do
  #Método equipo list é que checa o dashboard
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome] #have_content procura o que contém
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Então("deve conter a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to have_text expect_alert
end

# Remover anúncios
Dado('que eu tenho um anúncio indesejado:') do |table|
  user_id = page.execute_script("return localStorage.getItem('user')")
  # execute_script: executa um script dentro do ()
  # O script está pegando o ID do usuário e me retornando
  #log user_id

 thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb") #Abre a imagem
 #file_name está encapsulado dentro do método
 #Rb quer dizer somente leitura no formato binário. Ele encaminha todas as informações do thumbnail

  @equipo = {
    # table.rows_hash converte os valores da tabela em um objeto HASH
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
}

  EquiposService.new.create(@equipo, user_id)

  visit current_path #Carrega a página

end

Quando('eu solicito a exclusão desse item') do
  @dash_page.request_removal(@equipo[:name])
  # dash_page do Objetc. Request é o método que remove o equipo, ai eu passo como argumento o nome do equipo
  sleep 1 #Think time (Apenas para simular o usuário pensando)

end

Quando('confirmo a exclusão') do
  @dash_page.confirm_removal
  #Confirma a exclusão
end

Quando('não confirmo a solicitação') do
  @dash_page.cancel_removal
end

Então('não devo ver esse item no meu Dashboard') do
  expect(
    @dash_page.has_no_equipo?(@equipo[:name])
    ).to be true
end

Então('esse item deve permanecer no meu Dashboard') do
  expect(@dash_page.equipo_list).to have_content @equipo[:name] #have_content procura o que contém
end