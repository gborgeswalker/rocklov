Dado("que acesso a página de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|

  #log table.hashes

  user = table.hashes.first
  #armazeno na variável USERS, que vai ser igual a tabela. O HASHES transforma a tabela em um array.
  #First pega o primeiro do array

  #log user - Era apenas pra mostra os LOGS no terminal

  MongoDB.new.remove_user(user[:email]) #Chama a classe Mongo e encapsula a funcionalidade de excluir usuário

  @signup_page.create(user)
end
