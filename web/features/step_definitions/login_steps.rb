Dado("que acesso a página principal") do
  #@login_page = LoginPage.new #Foi incluida no hooks
  @login_page.open
end

Quando("submeto minhas credenciais com {string} e {string}") do |email, password| #troquei de string pra esses nomes
  #Login poge é a classe
  @login_page.with(email, password) # Email e password vem da featura
end
