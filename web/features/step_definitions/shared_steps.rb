Então("sou redirecionado para o Dashboard") do
  expect(@dash_page.on_dash?).to be true
  # Espera que o que o método "on_dash" retorno seja verdadeiro

end

Então("vejo a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to eql expect_alert
  #pega a classe alert, que está na alert.rb
  # O .dark chama o método que retorna o seletor css
  # To eql pegunta se é igual ao texto, que no caso é uma strin representada por expect_alert
end
