Dado('que meu perfil de anunciante é {string} e {string}') do |email, password|
  @email_anunciante = email
  @pass_anunciante = password


end

Dado('que eu tenho o seguinte equipamento cadastrado:') do |table|
  user_id = SessionsService.new.get_user_id(@email_anunciante, @pass_anunciante)
 #Está passando os dados do usuário
 

 thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb") #Abre a imagem
 #file_name está encapsulado dentro do método
 #Rb quer dizer somente leitura no formato binário. Ele encaminha todas as informações do thumbnail

  @equipo = {
    # table.rows_hash converte os valores da tabela em um objeto HASH
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
}
  MongoDB.new.remove_equipo(@equipo[:name], @email_anunciante)

  result = EquiposService.new.create(@equipo, user_id)
  @equipo_id = result.parsed_response["_id"] #Está retornando o ID do equipamento
  log @equipo_id
end

Dado('acesso o meu dashboard') do
  

  @login_page.open
  @login_page.with(@email_anunciante, @pass_anunciante) # Email e password vem da feature
  # Checkpoint para garantir que estamos no Dashboard
  expect(@dash_page.on_dash?).to be true
  # Espera que o que o método "on_dash" retorno seja verdadeiro

  
end

Quando('{string} e {string} solicita a locação desse equipo') do |email, password|
  user_id = SessionsService.new.get_user_id(email, password)#Está logando com dados do locatário
 #Está passando os dados do usuário
  EquiposService.new.booking(@equipo_id, user_id)

  #sleep 80
end

Então('devo ver a seguinte mensagem:') do |doc_string|
  expect_message = doc_string.gsub("DATA_ATUAL", Time.now.strftime("%d/%m/%Y"))
  # gsub é um recurso do ruby pra fazer substring
  #Código acima está substituindo o DATA ATUAL (Texto) pela data do dia da execfução
  
  expect(@dash_page.order).to have_text expect_message
end

Então('devo ver os links: {string} e {string} no pedido') do |button_accept, button_reject|
  # expect(page).to have_selector ".notifications button", text:button_accept
  # expect(page).to have_selector ".notifications button", text:button_reject
  #Código antes do page objects

  expect(@dash_page.order_actions(button_accept)).to be true
  expect(@dash_page.order_actions(button_reject)).to be true

end