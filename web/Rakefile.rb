require "digest/md5" # MD5 é um padrão de criptografia de senha
require_relative "features/support/libs/mongo"

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
  # Recebe uma senha string e retorna no formato MD5
end

task :local_seeds do
  CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/local.yml"))
  #Carrega o banco na pasta local (de ambiente)

  users = [
    { name: "Guilherme Borges", email: "gborges@gmail.com", password: to_md5("pwd123") },
    { name: "Anderson Silva", email: "spider@hotmail.com", password: to_md5("pwd123") },
    { name: "João Anunciante", email: "joao@anunciante.com", password: to_md5("pwd123") },
    { name: "Maria Locataria", email: "maria@locataria.com", password: to_md5("pwd123") },
  ]
  MongoDB.new.drop_danger #remove do banco
  MongoDB.new.insert_users(users) #Insere no banco

 sh "cucumber -p jenkins" #Carrega o cucumber
end

task :hmg_seeds do #Semente de homologação
  CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/hmg.yml"))

  users = [
    { name: "Guilherme Borges", email: "gborges@gmail.com", password: to_md5("pwd123") },
  ]
  MongoDB.new.drop_danger #remove do banco
  MongoDB.new.insert_users(users) #Insere no banco
end
